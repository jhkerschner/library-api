import { validateInput } from "./validation"; // Replace with the correct import path

describe("validateInput", () => {
  it("should return true for valid input", () => {
    const data = {
      name: "test",
      age: 1,
    };

    const schema = {
      name: "string",
      age: "number",
    };

    const result = validateInput(data, schema);
    expect(result).toBe(true);
  });

  it("should return false for missing properties", () => {
    const data = {
      name: "test",
    };

    const schema = {
      name: "string",
      age: "number",
    };

    const result = validateInput(data, schema);
    expect(result).toBe(false);
  });

  it("should return false for incorrect data types", () => {
    const data = {
      name: "John",
      age: "1",
    };

    const schema = {
      name: "string",
      age: "number",
    };

    const result = validateInput(data, schema);
    expect(result).toBe(false);
  });
});
