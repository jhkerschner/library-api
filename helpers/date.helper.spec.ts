import {
  convertMillisecondsToDate,
  getCurrentDate,
  getCurrentDateAndTwoWeeksOut,
} from "./date.helper"; // Replace with the correct import path

describe("convertMillisecondsToDate", () => {
  it("should convert milliseconds to date string", () => {
    const timestamp = Date.parse("2023-10-15T00:00:00");
    const result = convertMillisecondsToDate(timestamp);
    expect(result).toBe("2023-10-15");
  });
});

describe("getCurrentDate", () => {
  it("should return the current date at midnight", () => {
    const currentDate = getCurrentDate();
    const now = new Date();
    now.setHours(0, 0, 0, 0);
    const expectedTimestamp = now.getTime();
    expect(currentDate).toBe(expectedTimestamp);
  });
});

describe("getCurrentDateAndTwoWeeksOut", () => {
  it("should return current date and two weeks out", () => {
    const { currentDate, twoWeeksOut } = getCurrentDateAndTwoWeeksOut();
    const now = new Date();
    now.setHours(0, 0, 0, 0);

    const twoWeeks = 1000 * 60 * 60 * 24 * 7 * 2;
    const twoWeeksMock = currentDate + twoWeeks;

    const expectedTwoWeeksOut = new Date(twoWeeksMock);
    expectedTwoWeeksOut.setHours(23, 59, 59, 999);

    expect(currentDate).toBe(now.getTime());
    expect(twoWeeksOut).toBe(expectedTwoWeeksOut.getTime());
  });
});
