# Library API

This project is an API for a library built using the Serverless Framework, TypeScript, and SQLite as its database. It uses the Serverless Offline plugin to simulate AWS API Gateway, locally.

# Running the API

1. Checkout the Repo

2. Open the repo in terminal

3. run `npm install`

4. run `npm invoke:api` This will set up and pre populate the database and then launch the API for use.

By default, the serverless offline plugin uses ports 3000 and 3002 for http and lambda. If there are conflicts, you can change the ports in the serverless.yml file under the custom section.

```
custom:
	providerStage: ${opt:stage,'dev'}
	providerRegion: us-east-1
	serverless-offline:
		httpPort: 3000
		lambdaPort: 3002
```

5. There is a directory at the root of the project named "postman imports". There are three files to import (1 environment and 2 collections) to allow for easier testing of the API.

# Endpoints

Since there is no authentication or authorization, there are some endpoints that utilize the header to pass a userId. If there was authentication and authorization, I would have the user information in the context of the JWT token and parse it out from there for use in the function.

## Librarian (Admin) Endpoints

<details>

<summary><code>GET</code>  <code><b>/dev/admin/books</b></code></summary>

Returns a list of all books in the library

##### Parameters

> None

##### Responses

> | http code | content-type | response |

> | `200` | `application/json` | Object (JSON) |

</details>

<details>

<summary><code>GET</code>  <code><b>/dev/admin/books/overdue</b></code></summary>

Returns a list of all overdue books

##### Parameters

> None

##### Responses

> | http code | content-type | response |

> | `200` | `application/json` | Object (JSON) |

</details>

<details>

<summary><code>POST</code>  <code><b>/dev/admin/book</b></code>  </summary>

Creates a new book in library

##### Body Parameters

> | name | type | data type | description |

> | isbn | required | string | ISBN for the book. 13 digit code with hyphen |

> | author | required | string | Author name |

> | title | required | string | Book title |

> | description | required | string | Book description |

##### Responses

> | http code | content-type | response |

> | `200` | `application/json` | object of newly created book |

> | `400` | `application/json` | `{"code":"400","message":"missing required fields"}` |

> | `500` | `application/json` | None |

</details>

<details>

<summary><code>POST</code>  <code><b>/dev/admin/book/{id}</b></code>  </summary>

deletes a book by id in the library

Creates a new book in library

##### Path Parameters

> | name | type | data type | description |

> | id | required | number | id of the book |

##### Responses

> | http code | content-type | response |

> | `204` | `application/json` | None |

> | `400` | `application/json` | `{"code":"400","message":""}` |

> | `500` | `application/json` | None |

</details>

## User Endpoints

<details>

<summary><code>GET</code>  <code><b>/dev/user/books/checked-out</b></code></summary>

Returns a list of a users checked out books

##### Header Parameters

> | name | type | data type | description |

> | userId | required | number | id for the user |

> None

##### Responses

> | http code | content-type | response |

> | `200` | `application/json` | Object (JSON) containing a list of all books checked out for the user |

> | `400` | `application/json` | Object (JSON) Error message |

> | `500` | `application/json` | Object (JSON) Error message |

</details>

<details>

<summary><code>POST</code>  <code><b>/dev/user/book</b></code>  </summary>

Check out a new book

##### Body Parameters

> | name | type | data type | description |

> | isbn | required | string | ISBN for the book |

##### Responses

> | http code | content-type | response |

> | `200` | `application/json` | Object (JSON) of newly created book |

> | `409` | `application/json` | Object (JSON) Error message |

> | `500` | `application/json` | Object (JSON) Error message |

</details>

<details>

<summary><code>POST</code>  <code><b>/dev/user/book/return</b></code>  </summary>

Check out a new book

##### Body Parameters

> | name | type | data type | description |

> | isbn | required | string | ISBN for the book the user wants to return |

##### Responses

> | http code | content-type | response |

> | `200` | `application/json` | Object (JSON) Message |

> | `409` | `application/json` | Object (JSON) Error message |

> | `500` | `application/json` | Object (JSON) Error message |

</details>

# Tests

Each function as well as the helpers have tests written for them. To run the tests, you can run `npm run test`
