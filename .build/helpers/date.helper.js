"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCurrentDateAndTwoWeeksOut = exports.getCurrentDate = exports.convertMillisecondsToDate = void 0;
function convertMillisecondsToDate(timestamp) {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    return `${year}-${month}-${day}`;
}
exports.convertMillisecondsToDate = convertMillisecondsToDate;
function getCurrentDate() {
    const now = new Date(); // Get the current timestamp
    now.setHours(0, 0, 0, 0); // Set the time to midnight (00:00:00)
    return now.getTime(); // Return the timestamp
}
exports.getCurrentDate = getCurrentDate;
function getCurrentDateAndTwoWeeksOut() {
    const currentDate = getCurrentDate();
    const twoWeeks = 1000 * 60 * 60 * 24 * 7 * 2; // Two weeks in milliseconds
    const twoWeeksOut = currentDate + twoWeeks;
    // Set the time to 11:59:59 PM
    const twoWeeksOutDate = new Date(twoWeeksOut);
    twoWeeksOutDate.setHours(23, 59, 59, 999);
    return { currentDate: currentDate, twoWeeksOut: twoWeeksOutDate.getTime() };
}
exports.getCurrentDateAndTwoWeeksOut = getCurrentDateAndTwoWeeksOut;
