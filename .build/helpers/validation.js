"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateInput = void 0;
function validateInput(data, schema) {
    // Check if the data is an object
    if (typeof data !== "object") {
        return false;
    }
    // Check if the data has all of the required properties
    for (const key of Object.keys(schema)) {
        if (!data.hasOwnProperty(key)) {
            return false;
        }
    }
    // Check if the data types of the properties are correct
    for (const key of Object.keys(schema)) {
        const propertyType = schema[key];
        // Check if the property is a string
        if (propertyType === "string") {
            if (typeof data[key] !== "string") {
                return false;
            }
        }
        // Check if the property is a number
        if (propertyType === "number") {
            if (typeof data[key] !== "number") {
                return false;
            }
        }
        // Check if the property is a boolean
        if (propertyType === "boolean") {
            if (typeof data[key] !== "boolean") {
                return false;
            }
        }
    }
    return true;
}
exports.validateInput = validateInput;
