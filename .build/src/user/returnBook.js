"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const http_1 = require("../../common/http");
const book_1 = require("../../common/models/book");
const validation_1 = require("../../helpers/validation");
const transactions_1 = require("../../common/accessors/transactions");
function handler(event) {
    return __awaiter(this, void 0, void 0, function* () {
        if (typeof event === "undefined") {
            throw new Error("Event is not defined");
        }
        if (typeof event.headers.userId === "undefined") {
            return (0, http_1.return400)({ message: "A user ID is required" });
        }
        const userId = +event.headers.userId;
        let book;
        if (typeof event.body === "string") {
            try {
                book = JSON.parse(event.body);
            }
            catch (error) {
                throw new Error(String(error));
            }
        }
        else {
            book = event.body;
        }
        if (!book) {
            throw new Error("body is a required argument");
        }
        if (!(0, validation_1.validateInput)(book, book_1.BookReturn)) {
            return (0, http_1.return400)({ message: "Missing required fields" });
        }
        const isbn = book.isbn;
        //check is user has this book checked out and it's not already returned
        if (!(yield (0, transactions_1.doesUserHaveBook)(isbn, userId))) {
            return (0, http_1.return409)({ message: "Unable to complete request." });
        }
        const returnBook = yield (0, transactions_1.returnBookByIsbnAndUserId)(isbn, userId);
        if (returnBook) {
            return (0, http_1.return200)({ message: "Your book has been returned!" });
        }
        else {
            return (0, http_1.return500)({
                error: "An error occurred while checking out your book, please try again.",
            });
        }
    });
}
exports.handler = handler;
