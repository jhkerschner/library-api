"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const http_1 = require("../../common/http");
const book_1 = require("../../common/models/book");
const validation_1 = require("../../helpers/validation");
const transactions_1 = require("../../common/accessors/transactions");
const date_helper_1 = require("../../helpers/date.helper");
function handler(event) {
    return __awaiter(this, void 0, void 0, function* () {
        if (typeof event === "undefined") {
            throw new Error("Event is not defined");
        }
        if (typeof event.headers.userId === "undefined") {
            return (0, http_1.return400)({ message: "A user ID is required" });
        }
        const userId = +event.headers.userId;
        let book;
        if (typeof event.body === "string") {
            try {
                book = JSON.parse(event.body);
            }
            catch (error) {
                throw new Error(String(error));
            }
        }
        else {
            book = event.body;
        }
        if (!book) {
            throw new Error("body is a required argument");
        }
        if (!(0, validation_1.validateInput)(book, book_1.BookCheckOut)) {
            return (0, http_1.return400)({ message: "Missing required fields" });
        }
        const isbn = book.isbn;
        //check to see if the user has the book checked out already
        const doesUserHaveBook = yield (0, transactions_1.doesUserHaveBookCheckedOut)(userId, isbn);
        if (doesUserHaveBook) {
            return (0, http_1.return409)({
                error: "Unable to check out book, you already have this book checked out.",
            });
        }
        //check if book is available
        const availableBookId = yield (0, transactions_1.getAvailableBookIdByISBN)(isbn);
        if (availableBookId === null) {
            return (0, http_1.return409)({
                error: "This book is not available for checking out at this time",
            });
        }
        //check to see how many books the user has checked out
        const checkedOutBooks = yield (0, transactions_1.getCheckedOutBooksByUserId)(userId);
        if (checkedOutBooks >= 3) {
            return (0, http_1.return409)({
                error: "You have 3 books checked out and are unable to check out more. Please return your books to check out more.",
            });
        }
        //check for overdue books
        const overDueBooks = yield (0, transactions_1.getOverdueBooksByUserId)(userId);
        if (overDueBooks > 0) {
            return (0, http_1.return409)({
                error: "You have at least one book overdue. You need to return the book(s) before you can check out more books.",
            });
        }
        const { currentDate, twoWeeksOut } = (0, date_helper_1.getCurrentDateAndTwoWeeksOut)();
        //check out the requested book
        const data = {
            bookId: availableBookId,
            isbn,
            userId,
            checkOutDate: currentDate,
            dueBackDate: twoWeeksOut,
        };
        const newTransaction = yield (0, transactions_1.checkOutBook)(data);
        if (newTransaction) {
            return (0, http_1.return200)({ data: newTransaction });
        }
        else {
            return (0, http_1.return500)({
                error: "An error occurred while checking out your book, please try again.",
            });
        }
    });
}
exports.handler = handler;
