"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const http_1 = require("../../common/http");
const book_1 = require("../../common/models/book");
const validation_1 = require("../../helpers/validation");
const books_1 = require("../../common/accessors/books");
function handler(event) {
    return __awaiter(this, void 0, void 0, function* () {
        if (typeof event === "undefined") {
            throw new Error("Event is not defined");
        }
        let book;
        if (typeof event.body === "string") {
            try {
                book = JSON.parse(event.body);
            }
            catch (error) {
                throw new Error(String(error));
            }
        }
        else {
            book = event.body;
        }
        if (!book) {
            throw new Error("body is a required argument");
        }
        if (!(0, validation_1.validateInput)(book, book_1.BookCreate)) {
            return (0, http_1.return400)({ message: "Missing required fields," });
        }
        const newBook = yield (0, books_1.createBook)(book);
        if (newBook) {
            return (0, http_1.return200)({ data: newBook });
        }
        else {
            return (0, http_1.return500)({
                error: "An error occurred while saving the book, please try again.",
            });
        }
    });
}
exports.handler = handler;
