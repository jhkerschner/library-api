"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const http_1 = require("../../common/http");
const transactions_1 = require("../../common/accessors/transactions");
function handler(event) {
    return __awaiter(this, void 0, void 0, function* () {
        if (typeof event === "undefined") {
            throw new Error("Event is not defined");
        }
        const allOverDueBooks = yield (0, transactions_1.getAllOverdueBooks)();
        if (allOverDueBooks) {
            return (0, http_1.return200)({ data: allOverDueBooks });
        }
        else {
            return (0, http_1.return500)({
                error: "An error occurred while retrieving overdue books, please try again.",
            });
        }
    });
}
exports.handler = handler;
