"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const http_1 = require("../../common/http");
const books_1 = require("../../common/accessors/books");
function handler(event) {
    return __awaiter(this, void 0, void 0, function* () {
        if (typeof event === "undefined") {
            throw new Error("Event is not defined");
        }
        if (!event.pathParameters) {
            return (0, http_1.return400)({ error: "pathParameters not set" });
        }
        if (typeof event.pathParameters.id === "undefined") {
            return (0, http_1.return400)({ error: "id is not set in pathParameters" });
        }
        const bookId = +event.pathParameters.id;
        const deleteBook = yield (0, books_1.deleteBookById)(bookId);
        if (deleteBook) {
            return (0, http_1.return204)({ data: {} });
        }
        else {
            return (0, http_1.return500)({
                error: "An error occurred while deleting the book, please try again.",
            });
        }
    });
}
exports.handler = handler;
