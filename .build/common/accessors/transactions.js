"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAvailableBookIdByISBN = exports.doesUserHaveBookCheckedOut = exports.getAllOverdueBooks = exports.isBookCheckedOut = exports.getAllCheckedOutBooksByUserId = exports.returnBookByIsbnAndUserId = exports.checkOutBook = exports.doesUserHaveBook = exports.getOverdueBooksByUserId = exports.getCheckedOutBooksByUserId = exports.getAllTransactions = void 0;
const sequelize_1 = require("sequelize");
const transaction_1 = __importDefault(require("../models/transaction"));
const book_1 = __importDefault(require("../models/book"));
const books_1 = require("./books");
const date_helper_1 = require("../../helpers/date.helper");
const user_1 = __importDefault(require("../models/user"));
function getAllTransactions() {
    return __awaiter(this, void 0, void 0, function* () {
        const transactions = yield transaction_1.default.findAll();
        if (transactions) {
            return transaction_1.default.toData(transactions);
        }
    });
}
exports.getAllTransactions = getAllTransactions;
function getCheckedOutBooksByUserId(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const checkedOutBooks = yield transaction_1.default.count({
                where: {
                    userId,
                    returned: false,
                },
            });
            return checkedOutBooks;
        }
        catch (error) {
            throw new Error(`Error while retrieving checked-out books: ${error}`);
        }
    });
}
exports.getCheckedOutBooksByUserId = getCheckedOutBooksByUserId;
function getOverdueBooksByUserId(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const currentDate = (0, date_helper_1.getCurrentDate)();
            const overdueBooksCount = yield transaction_1.default.count({
                where: {
                    userId,
                    dueBackDate: {
                        [sequelize_1.Op.lt]: currentDate,
                    },
                },
            });
            return overdueBooksCount;
        }
        catch (error) {
            console.error("Error getting overdue books:", error);
            throw error;
        }
    });
}
exports.getOverdueBooksByUserId = getOverdueBooksByUserId;
function doesUserHaveBook(isbn, userId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const count = yield transaction_1.default.count({
                where: {
                    isbn: isbn,
                    userId: userId,
                    returned: false, // Only count transactions where the book hasn't been returned
                },
            });
            return count;
        }
        catch (error) {
            console.error("Error checking if the user has the book:", error);
            throw error;
        }
    });
}
exports.doesUserHaveBook = doesUserHaveBook;
function checkOutBook(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const newTransaction = yield transaction_1.default.create(data);
            if (newTransaction) {
                const transaction = transaction_1.default.toData([
                    newTransaction,
                ]);
                let book = yield (0, books_1.getBookById)(data.bookId);
                if (transaction && book) {
                    return {
                        id: book[0].id,
                        isbn: book[0].isbn,
                        author: book[0].author,
                        title: book[0].title,
                        description: book[0].description,
                        checkOutDate: (0, date_helper_1.convertMillisecondsToDate)(transaction[0].checkOutDate),
                        dueBackDate: (0, date_helper_1.convertMillisecondsToDate)(transaction[0].dueBackDate),
                    };
                }
                else {
                    throw new Error("Unable to check out book.");
                }
            }
            else {
                throw new Error("Unable to check out book.");
            }
        }
        catch (error) {
            throw new Error(`Error checking out book: ${error.message}`);
        }
    });
}
exports.checkOutBook = checkOutBook;
function returnBookByIsbnAndUserId(isbn, userId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const updatedTransaction = yield transaction_1.default.update({ returned: true }, {
                where: { isbn, userId },
                returning: true,
            });
            if (updatedTransaction) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (error) {
            console.error("Error returning book:", error);
            throw error;
        }
    });
}
exports.returnBookByIsbnAndUserId = returnBookByIsbnAndUserId;
function getAllCheckedOutBooksByUserId(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const checkedOutBooks = yield transaction_1.default.findAll({
                where: {
                    userId: userId,
                    returned: false,
                },
                include: {
                    model: book_1.default,
                    as: "book",
                },
            });
            return checkedOutBooks.map((transaction) => transaction.book);
        }
        catch (error) {
            console.error("Error getting checked-out books by user:", error);
            throw error;
        }
    });
}
exports.getAllCheckedOutBooksByUserId = getAllCheckedOutBooksByUserId;
function isBookCheckedOut(isbn) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const transaction = yield transaction_1.default.findOne({
                where: {
                    isbn: isbn,
                    returned: false,
                },
            });
            return !!transaction;
        }
        catch (error) {
            console.error("Error checking if the book is checked out:", error);
            throw error;
        }
    });
}
exports.isBookCheckedOut = isBookCheckedOut;
function getAllOverdueBooks() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const currentDate = (0, date_helper_1.getCurrentDate)();
            const overdueBooks = yield transaction_1.default.findAll({
                where: {
                    dueBackDate: { [sequelize_1.Op.lt]: currentDate },
                    returned: false,
                },
                include: [
                    {
                        model: book_1.default,
                        as: "book",
                    },
                    {
                        model: user_1.default,
                        as: "user",
                    },
                ],
            });
            return overdueBooks.map((transaction) => ({
                id: transaction.book.id,
                isbn: transaction.book.isbn,
                author: transaction.book.author,
                title: transaction.book.title,
                description: transaction.book.description,
                checkOutDate: (0, date_helper_1.convertMillisecondsToDate)(transaction.checkOutDate),
                dueBackDate: (0, date_helper_1.convertMillisecondsToDate)(transaction.dueBackDate),
                username: transaction.user.username,
            }));
        }
        catch (error) {
            console.error("Error getting overdue books:", error);
            throw error;
        }
    });
}
exports.getAllOverdueBooks = getAllOverdueBooks;
function doesUserHaveBookCheckedOut(userId, isbn) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const unreturnedTransaction = yield transaction_1.default.count({
                where: {
                    userId: userId,
                    isbn: isbn,
                    returned: false,
                },
            });
            return unreturnedTransaction; // Returns true if an unreturned transaction exists, false otherwise
        }
        catch (error) {
            console.error("Error checking if the user has a book checked out:", error);
            throw error;
        }
    });
}
exports.doesUserHaveBookCheckedOut = doesUserHaveBookCheckedOut;
function getAvailableBookIdByISBN(isbn) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const unreturnedTransactionIds = yield transaction_1.default.findAll({
                where: {
                    returned: false,
                },
                attributes: ["bookId"],
                include: [
                    {
                        model: book_1.default,
                        as: "book",
                        where: { isbn: isbn },
                    },
                ],
            });
            const idsOfBooksThatAreOut = unreturnedTransactionIds.map((transaction) => transaction.bookId);
            const availableBook = yield book_1.default.findOne({
                where: {
                    isbn: isbn,
                    id: { [sequelize_1.Op.notIn]: idsOfBooksThatAreOut },
                },
            });
            return availableBook ? availableBook.id : null;
        }
        catch (error) {
            console.error("Error getting available book ID by ISBN:", error);
            throw error;
        }
    });
}
exports.getAvailableBookIdByISBN = getAvailableBookIdByISBN;
