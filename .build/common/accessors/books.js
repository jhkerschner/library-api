"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteBookById = exports.updateBook = exports.createBook = exports.getBookCountByISBN = exports.getBookById = exports.getAllBooks = void 0;
const book_1 = __importDefault(require("../models/book"));
function getAllBooks() {
    return __awaiter(this, void 0, void 0, function* () {
        const books = yield book_1.default.findAll();
        if (books) {
            return book_1.default.toData(books);
        }
        else {
            throw new Error("No Books Found");
        }
    });
}
exports.getAllBooks = getAllBooks;
function getBookById(bookId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const book = yield book_1.default.findByPk(bookId);
            if (book) {
                return book_1.default.toData([book]);
            }
            else {
                return [];
            }
        }
        catch (error) {
            console.error("Error getting book by ID:", error);
            throw error;
        }
    });
}
exports.getBookById = getBookById;
function getBookCountByISBN(isbn) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const bookCount = yield book_1.default.count({
                where: { isbn: isbn },
            });
            return bookCount;
        }
        catch (error) {
            console.error("Error getting book count by ISBN:", error);
            throw error;
        }
    });
}
exports.getBookCountByISBN = getBookCountByISBN;
function createBook(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const newBook = yield book_1.default.create(data);
            if (newBook) {
                return book_1.default.toData([newBook]);
            }
            else {
                throw new Error("Unable to create new book.");
            }
        }
        catch (error) {
            throw new Error(`Error creating new book: ${error.message}`);
        }
    });
}
exports.createBook = createBook;
function updateBook(bookId, updatedData) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const book = yield book_1.default.findByPk(bookId);
            if (!book) {
                throw new Error(`Book with ID ${bookId} not found`);
            }
            // Update the book's attributes with the provided data
            yield book.update(updatedData);
            // Return the updated book
            return book;
        }
        catch (error) {
            console.error("Error updating the book:", error);
            throw error;
        }
    });
}
exports.updateBook = updateBook;
function deleteBookById(bookId) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            // Use Sequelize's destroy method to delete the book by its ID
            const deletedRows = yield book_1.default.destroy({
                where: {
                    id: bookId,
                },
            });
            if (deletedRows === 0) {
                // If no rows were deleted, the book was not found
                throw new Error(`Book with ID ${bookId} not found.`);
            }
            return true;
        }
        catch (error) {
            // Handle any errors that may occur during the deletion process
            throw new Error(`Error deleting the book: ${error.message}`);
        }
    });
}
exports.deleteBookById = deleteBookById;
