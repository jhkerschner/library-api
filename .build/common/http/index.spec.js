"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable max-lines-per-function */
const index_1 = require("./index");
const mockBody = {
    shipClasses: [
        'Sovereign',
        'Galaxy',
        'Ambassador',
        'Excelsior',
        'Constitution',
        'Prometheus',
    ],
    show: 'Star Trek',
};
describe('Http Helpers', () => {
    it('should return 200', () => {
        const actual = (0, index_1.return200)(mockBody);
        const expected = {
            body: JSON.stringify(mockBody),
            headers: {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Origin': '*',
            },
            statusCode: 200,
        };
        expect(actual).toEqual(expected);
    });
    it('should return 401', () => {
        const actual = (0, index_1.return401)(mockBody);
        const expected = {
            body: JSON.stringify(mockBody),
            headers: {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Origin': '*',
            },
            statusCode: 401,
        };
        expect(actual).toEqual(expected);
    });
    it('should return 403', () => {
        const actual = (0, index_1.return403)(mockBody);
        const expected = {
            body: JSON.stringify(mockBody),
            headers: {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Origin': '*',
            },
            statusCode: 403,
        };
        expect(actual).toEqual(expected);
    });
    it('should return 404', () => {
        const actual = (0, index_1.return404)(mockBody);
        const expected = {
            body: JSON.stringify(mockBody),
            headers: {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Origin': '*',
            },
            statusCode: 404,
        };
        expect(actual).toEqual(expected);
    });
    it('should return 422', () => {
        const actual = (0, index_1.return422)(mockBody);
        const expected = {
            body: JSON.stringify(mockBody),
            headers: {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Origin': '*',
            },
            statusCode: 422,
        };
        expect(actual).toEqual(expected);
    });
    it('should return 500', () => {
        const actual = (0, index_1.return500)(mockBody);
        const expected = {
            body: JSON.stringify(mockBody),
            headers: {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Origin': '*',
            },
            statusCode: 500,
        };
        expect(actual).toEqual(expected);
    });
    it('should return 200 manually', () => {
        const actual = (0, index_1.returnHttp)(200, mockBody);
        const expected = {
            body: JSON.stringify(mockBody),
            headers: {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Origin': '*',
            },
            statusCode: 200,
        };
        expect(actual).toEqual(expected);
    });
});
