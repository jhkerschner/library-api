"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.returnHttp = exports.return500 = exports.return422 = exports.return409 = exports.return404 = exports.return403 = exports.return401 = exports.return400 = exports.return204 = exports.return200 = void 0;
function return200(body) {
    return returnHttp(200, body);
}
exports.return200 = return200;
function return204(body) {
    return returnHttp(204, body);
}
exports.return204 = return204;
function return400(body) {
    return returnHttp(400, body);
}
exports.return400 = return400;
function return401(body) {
    return returnHttp(401, body);
}
exports.return401 = return401;
function return403(body) {
    return returnHttp(403, body);
}
exports.return403 = return403;
function return404(body) {
    return returnHttp(404, body);
}
exports.return404 = return404;
function return409(body) {
    return returnHttp(409, body);
}
exports.return409 = return409;
// Unprocessable Entity - You Done Messed Up A-Aron!
function return422(body) {
    return returnHttp(422, body);
}
exports.return422 = return422;
function return500(body) {
    return returnHttp(500, body);
}
exports.return500 = return500;
function returnHttp(statusCode, body) {
    return {
        body: JSON.stringify(body),
        headers: {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Origin": "*",
        },
        statusCode,
    };
}
exports.returnHttp = returnHttp;
