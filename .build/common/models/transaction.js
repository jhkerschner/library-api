"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const sequelize_2 = __importDefault(require("./sequelize"));
const book_1 = __importDefault(require("./book"));
const user_1 = __importDefault(require("./user"));
class Transactions extends sequelize_1.Model {
    static toData(users) {
        return users.map((transaction) => ({
            id: transaction.id,
            bookId: transaction.bookId,
            isbn: transaction.isbn,
            userId: transaction.userId,
            checkOutDate: transaction.checkOutDate,
            dueBackDate: transaction.dueBackDate,
            returned: transaction.returned,
        }));
    }
}
Transactions.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    bookId: {
        type: sequelize_1.DataTypes.NUMBER,
        allowNull: false,
        references: {
            model: "books",
            key: "id",
        },
    },
    isbn: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        validate: {
            isISBN(value) {
                const cleanedISBN = value.replace(/[-\s]/g, "");
                const test = /^\d{13}$/.test(cleanedISBN);
                if (!test) {
                    throw new Error("Invalid ISBN-13");
                }
            },
        },
    },
    userId: {
        type: sequelize_1.DataTypes.NUMBER,
        allowNull: false,
        references: {
            model: "users",
            key: "id",
        },
    },
    checkOutDate: {
        type: sequelize_1.DataTypes.NUMBER,
        allowNull: true,
    },
    dueBackDate: {
        type: sequelize_1.DataTypes.NUMBER,
        allowNull: true,
    },
    returned: {
        type: sequelize_1.DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
}, {
    sequelize: sequelize_2.default,
    tableName: "transactions",
    timestamps: true,
});
Transactions.belongsTo(book_1.default, {
    foreignKey: "bookId",
    as: "book",
});
Transactions.belongsTo(user_1.default, {
    foreignKey: "userId",
    as: "user",
});
exports.default = Transactions;
