"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//const { roles } = require("../../config");
const sequelize_1 = require("sequelize");
const sequelize_2 = __importDefault(require("./sequelize"));
class User extends sequelize_1.Model {
    static toData(users) {
        return users.map((user) => ({
            id: user.id,
            username: user.username,
            userType: user.userType,
        }));
    }
}
User.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    username: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    userType: {
        type: sequelize_1.DataTypes.ENUM("librarian", "user"),
        allowNull: false,
    },
}, {
    sequelize: sequelize_2.default,
    tableName: "users",
    timestamps: true,
});
exports.default = User;
