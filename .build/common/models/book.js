"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookReturn = exports.BookCheckOut = exports.BookCreate = void 0;
const sequelize_1 = require("sequelize");
const sequelize_2 = __importDefault(require("./sequelize"));
exports.BookCreate = {
    isbn: "string",
    author: "string",
    title: "string",
    description: "string",
};
exports.BookCheckOut = {
    isbn: "string",
};
exports.BookReturn = {
    isbn: "string",
};
class Book extends sequelize_1.Model {
    static toData(books) {
        return books.map((book) => ({
            id: book.id,
            isbn: book.isbn,
            author: book.author,
            title: book.title,
            description: book.description,
        }));
    }
}
Book.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    isbn: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        validate: {
            isISBN(value) {
                const cleanedISBN = value.replace(/[-\s]/g, "");
                const test = /^\d{13}$/.test(cleanedISBN);
                if (!test) {
                    throw new Error("Invalid ISBN-13");
                }
            },
        },
    },
    author: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    title: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
}, {
    sequelize: sequelize_2.default,
    tableName: "books",
    timestamps: true,
});
exports.default = Book;
