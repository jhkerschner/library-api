"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const book_1 = __importDefault(require("./common/models/book"));
const sequelize_1 = __importDefault(require("./common/models/sequelize"));
const transaction_1 = __importDefault(require("./common/models/transaction"));
const user_1 = __importDefault(require("./common/models/user"));
const date_helper_1 = require("./helpers/date.helper");
function handler() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield sequelize_1.default.authenticate();
            yield sequelize_1.default.sync({ force: true });
            //Add user records
            yield user_1.default.create({ username: "Librarian", userType: "librarian" });
            yield user_1.default.create({ username: "User 1", userType: "user" });
            yield user_1.default.create({ username: "User 2", userType: "user" });
            yield user_1.default.create({ username: "User 3", userType: "user" });
            //Add book records
            yield book_1.default.create({
                isbn: "978-1492057017",
                author: "Jason Katzer",
                title: "Learning Serverless: Design, Develop, and Deploy with Confidence 1st Edition",
                description: "Whether your company is considering serverless computing or has already made the decision to adopt this model, this practical book is for you. Author Jason Katzer shows early and mid-career developers what's required to build and ship maintainable and scalable services using this model.",
            });
            yield book_1.default.create({
                isbn: "978-1492057017",
                author: "Jason Katzer",
                title: "Learning Serverless: Design, Develop, and Deploy with Confidence 1st Edition",
                description: "Whether your company is considering serverless computing or has already made the decision to adopt this model, this practical book is for you. Author Jason Katzer shows early and mid-career developers what's required to build and ship maintainable and scalable services using this model.",
            });
            yield book_1.default.create({
                isbn: "978-1492057017",
                author: "Jason Katzer",
                title: "Learning Serverless: Design, Develop, and Deploy with Confidence 1st Edition",
                description: "Whether your company is considering serverless computing or has already made the decision to adopt this model, this practical book is for you. Author Jason Katzer shows early and mid-career developers what's required to build and ship maintainable and scalable services using this model.",
            });
            yield book_1.default.create({
                isbn: "978-1492037651",
                author: "Boris Cherny",
                title: "Programming TypeScript: Making Your JavaScript Applications Scale 1st Edition",
                description: "Any programmer working with a dynamically typed language will tell you how hard it is to scale to more lines of code and more engineers. That’s why Facebook, Google, and Microsoft invented gradual static type layers for their dynamically typed JavaScript and Python code. This practical book shows you how one such type layer, TypeScript, is unique among them: it makes programming fun with its powerful static type system.",
            });
            //Add transaction recrods
            const dates = (0, date_helper_1.getCurrentDateAndTwoWeeksOut)();
            yield transaction_1.default.create({
                bookId: 1,
                isbn: "978-1492057017",
                userId: 2,
                checkOutDate: dates.currentDate,
                dueBackDate: dates.twoWeeksOut,
            });
            yield transaction_1.default.create({
                bookId: 1,
                isbn: "978-1492057017",
                userId: 3,
                checkOutDate: "1696996800000",
                dueBackDate: "1698292799999",
            });
            console.log("Database synchronized.");
        }
        catch (error) {
            console.error("Error connecting to the database:", error);
        }
    });
}
exports.handler = handler;
