import { handler } from "./returnBook"; // Replace with the correct import path
import {
  doesUserHaveBook,
  returnBookByIsbnAndUserId,
} from "../../common/accessors/transactions";
const returnBookRequest: any = require("../../mock/returnBook.mock.json");

jest.mock("../../common/http", () => {
  return {
    return200: (): any => {
      return { message: "Your book has been returned!" };
    },
    return400: (): any => {
      return { message: "A user ID is required" };
    },
    return409: (): any => {
      return { message: "Unable to complete request." };
    },
    return500: (): any => {
      return {
        error: "An error occurred while returning your book, please try again.",
      };
    },
  };
});

jest.mock("../../common/accessors/transactions", () => {
  return {
    doesUserHaveBook: jest.fn(),
    returnBookByIsbnAndUserId: jest.fn(),
  };
});

describe("handler", () => {
  it("should return a 200 response when the book is successfully returned", async () => {
    const doesUserHaveBookMock = doesUserHaveBook as jest.Mock;
    doesUserHaveBookMock.mockResolvedValue(true);

    const returnBookByIsbnAndUserIdMock =
      returnBookByIsbnAndUserId as jest.Mock;
    returnBookByIsbnAndUserIdMock.mockResolvedValue(true);

    const result = await handler(returnBookRequest);

    expect(result).toEqual({ message: "Your book has been returned!" });
  });

  it("should return a 400 response when the user ID is missing", async () => {
    const event = returnBookRequest;
    event.headers.userId = undefined;
    const result = await handler(event);

    expect(result).toEqual({ message: "A user ID is required" });
  });

  it("should return a 409 response when the user has the book or the book is already returned", async () => {
    const doesUserHaveBookMock = doesUserHaveBook as jest.Mock;
    doesUserHaveBookMock.mockResolvedValue(false); // User has the book
    const event = returnBookRequest;
    event.headers.userId = 2;

    const result = await handler(event);

    expect(result).toEqual({ message: "Unable to complete request." });
  });

  it("should return a 500 response when an error occurs during book return", async () => {
    const doesUserHaveBookMock = doesUserHaveBook as jest.Mock;
    doesUserHaveBookMock.mockResolvedValue(true);

    const returnBookByIsbnAndUserIdMock =
      returnBookByIsbnAndUserId as jest.Mock;
    returnBookByIsbnAndUserIdMock.mockResolvedValue(false);

    const result = await handler(returnBookRequest);

    expect(result).toEqual({
      error: "An error occurred while returning your book, please try again.",
    });
  });
});
