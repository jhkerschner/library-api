import { handler } from "./getUsersCheckedOutBooks"; // Replace with the correct import path
import { getAllCheckedOutBooksByUserId } from "../../common/accessors/transactions";
const getUsersCheckedOutBooksRequest: any = require("../../mock/getUsersCheckedOutBooks.mock.json");

const mockCheckedOutBooks = [
  {
    id: 1,
    isbn: "978-1492057017",
    author: "Jason Katzer",
    title:
      "Learning Serverless: Design, Develop, and Deploy with Confidence 1st Edition",
    description:
      "Whether your company is considering serverless computing or has already made the decision to adopt this model, this practical book is for you. Author Jason Katzer shows early and mid-career developers what's required to build and ship maintainable and scalable services using this model.",
    createdAt: "2023-10-27T13:25:53.528Z",
    updatedAt: "2023-10-27T13:25:53.528Z",
  },
  {
    id: 3,
    isbn: "978-1492037651",
    author: "Boris Cherny",
    title:
      "Programming TypeScript: Making Your JavaScript Applications Scale 1st Edition",
    description:
      "Any programmer working with a dynamically typed language will tell you how hard it is to scale to more lines of code and more engineers. That’s why Facebook, Google, and Microsoft invented gradual static type layers for their dynamically typed JavaScript and Python code. This practical book shows you how one such type layer, TypeScript, is unique among them: it makes programming fun with its powerful static type system.",
    createdAt: "2023-10-27T13:25:53.531Z",
    updatedAt: "2023-10-27T13:25:53.531Z",
  },
];

jest.mock("../../common/http", () => {
  return {
    return200: (): any => {
      return { data: mockCheckedOutBooks };
    },
    return400: (): any => {
      return { message: "A user ID is required" };
    },
    return500: (): any => {
      return {
        error:
          "An error occurred while retrieving checked out books, please try again.",
      };
    },
  };
});

jest.mock("../../common/accessors/transactions", () => {
  return {
    getAllCheckedOutBooksByUserId: jest.fn(),
  };
});

describe("handler", () => {
  it("should return a 200 response with checked out books", async () => {
    const getAllCheckedOutBooksByUserIdMock =
      getAllCheckedOutBooksByUserId as jest.Mock;
    getAllCheckedOutBooksByUserIdMock.mockResolvedValue(mockCheckedOutBooks); // Provide the expected data

    const actual = await handler(getUsersCheckedOutBooksRequest);

    expect(actual).toEqual({ data: mockCheckedOutBooks }); // Adjust the expected data as needed
  });

  it("should return a 400 response when the user ID is missing", async () => {
    const event = getUsersCheckedOutBooksRequest;
    event.headers.userId = undefined;

    const actual = await handler(getUsersCheckedOutBooksRequest);

    expect(actual).toEqual({ message: "A user ID is required" });
  });

  it("should return a 500 response when an error occurs during book retrieval", async () => {
    const getAllCheckedOutBooksByUserIdMock =
      getAllCheckedOutBooksByUserId as jest.Mock;
    getAllCheckedOutBooksByUserIdMock.mockResolvedValue(false);

    const event = getUsersCheckedOutBooksRequest;
    event.headers.userId = 2;
    const actual = await handler(event);

    expect(actual).toEqual({
      error:
        "An error occurred while retrieving checked out books, please try again.",
    });
  });
});
