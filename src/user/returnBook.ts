import { return200, return400, return409, return500 } from "../../common/http";
import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { BookReturn } from "../../common/models/book";
import { validateInput } from "../../helpers/validation";
import {
  doesUserHaveBook,
  returnBookByIsbnAndUserId,
} from "../../common/accessors/transactions";

export async function handler(
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> {
  if (typeof event === "undefined") {
    throw new Error("Event is not defined");
  }

  if (typeof event.headers.userId === "undefined") {
    return return400({ message: "A user ID is required" });
  }
  const userId: number = +event.headers.userId;

  let book: BookReturn | null;
  if (typeof event.body === "string") {
    try {
      book = JSON.parse(event.body);
    } catch (error) {
      throw new Error(String(error));
    }
  } else {
    book = event.body;
  }

  if (!book) {
    throw new Error("body is a required argument");
  }

  if (!validateInput(book, BookReturn)) {
    return return400({ message: "Missing required fields" });
  }
  const isbn: string = book.isbn;

  //check is user has this book checked out and it's not already returned
  if (!(await doesUserHaveBook(isbn, userId))) {
    return return409({ message: "Unable to complete request." });
  }

  const returnBook: boolean = await returnBookByIsbnAndUserId(isbn, userId);

  if (returnBook) {
    return return200({ message: "Your book has been returned!" });
  } else {
    return return500({
      error:
        "An error occurred while checking out your book, please try again.",
    });
  }
}
