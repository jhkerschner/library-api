import { return200, return400, return409, return500 } from "../../common/http";
import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { BookCheckOut, CheckedOutBookModel } from "../../common/models/book";
import { validateInput } from "../../helpers/validation";
import {
  checkOutBook,
  getCheckedOutBooksByUserId,
  getOverdueBooksByUserId,
  getAvailableBookIdByISBN,
  doesUserHaveBookCheckedOut,
} from "../../common/accessors/transactions";
import { getCurrentDateAndTwoWeeksOut } from "../../helpers/date.helper";

export async function handler(
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> {
  if (typeof event === "undefined") {
    throw new Error("Event is not defined");
  }

  if (typeof event.headers.userId === "undefined") {
    return return400({ message: "A user ID is required" });
  }
  const userId: number = +event.headers.userId;

  let book: BookCheckOut | null;
  if (typeof event.body === "string") {
    try {
      book = JSON.parse(event.body);
    } catch (error) {
      throw new Error(String(error));
    }
  } else {
    book = event.body;
  }

  if (!book) {
    throw new Error("body is a required argument");
  }

  if (!validateInput(book, BookCheckOut)) {
    return return400({ message: "Missing required fields" });
  }

  const isbn: string = book.isbn;

  //check to see if the user has the book checked out already
  const doesUserHaveBook: number = await doesUserHaveBookCheckedOut(
    userId,
    isbn
  );

  if (doesUserHaveBook) {
    return return409({
      error:
        "Unable to check out book, you already have this book checked out.",
    });
  }

  //check if book is available
  const availableBookId: number | null = await getAvailableBookIdByISBN(isbn);
  if (availableBookId === null) {
    return return409({
      error: "This book is not available for checking out at this time",
    });
  }

  //check to see how many books the user has checked out
  const checkedOutBooks: number = await getCheckedOutBooksByUserId(userId);
  if (checkedOutBooks >= 3) {
    return return409({
      error:
        "You have 3 books checked out and are unable to check out more. Please return your books to check out more.",
    });
  }

  //check for overdue books
  const overDueBooks: number = await getOverdueBooksByUserId(userId);
  if (overDueBooks > 0) {
    return return409({
      error:
        "You have at least one book overdue. You need to return the book(s) before you can check out more books.",
    });
  }

  const { currentDate, twoWeeksOut } = getCurrentDateAndTwoWeeksOut();
  //check out the requested book
  const data = {
    bookId: availableBookId,
    isbn,
    userId,
    checkOutDate: currentDate,
    dueBackDate: twoWeeksOut,
  };

  const newTransaction: CheckedOutBookModel = await checkOutBook(data);

  if (newTransaction) {
    return return200({ data: newTransaction });
  } else {
    return return500({
      error:
        "An error occurred while checking out your book, please try again.",
    });
  }
}
