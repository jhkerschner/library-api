import { handler } from "./checkOutBook"; // Replace with the correct import path
import { return409 } from "../../common/http";
import {
  checkOutBook,
  doesUserHaveBookCheckedOut,
  getAvailableBookIdByISBN,
  getCheckedOutBooksByUserId,
  getOverdueBooksByUserId,
  isBookCheckedOut,
} from "../../common/accessors/transactions";
import { getBookById } from "../../common/accessors/books";
const mockCheckOutBookRequest: any = require("../../mock/checkOutBook.mock.json");

const mockBookData = [
  {
    id: 3,
    isbn: "978-1492037651",
    author: "Boris Cherny",
    title:
      "Programming TypeScript: Making Your JavaScript Applications Scale 1st Edition",
    description:
      "Any programmer working with a dynamically typed language will tell you how hard it is to scale to more lines of code and more engineers. That’s why Facebook, Google, and Microsoft invented gradual static type layers for their dynamically typed JavaScript and Python code. This practical book shows you how one such type layer, TypeScript, is unique among them: it makes programming fun with its powerful static type system.",
  },
];
const mockCheckOutData = {
  isbn: "978-1492037651",
  author: "Boris Cherny",
  title:
    "Programming TypeScript: Making Your JavaScript Applications Scale 1st Edition",
  description:
    "Any programmer working with a dynamically typed language will tell you how hard it is to scale to more lines of code and more engineers. That’s why Facebook, Google, and Microsoft invented gradual static type layers for their dynamically typed JavaScript and Python code. This practical book shows you how one such type layer, TypeScript, is unique among them: it makes programming fun with its powerful static type system.",
  checkOutDate: "2023-10-30",
  dueBackDate: "2023-11-12",
};

jest.mock("../../common/accessors/transactions", () => {
  return {
    checkOutBook: jest.fn(),
    getCheckedOutBooksByUserId: jest.fn(),
    getOverdueBooksByUserId: jest.fn(),
    doesUserHaveBookCheckedOut: jest.fn(),
    getAvailableBookIdByISBN: jest.fn(),
  };
});
jest.mock("../../common/accessors/books", () => {
  return {
    getBookById: jest.fn(),
  };
});

jest.mock("../../common/http", () => {
  return {
    return200: (): any => {
      return { data: mockCheckOutData };
    },
    return400: (): any => {
      return { message: "Missing required fields" };
    },
    return409: jest.fn(),
    return500: (): any => {
      return {
        error:
          "An error occurred while checking out your book, please try again.",
      };
    },
  };
});

describe("handler", () => {
  it("should return a 200 response when book is successfully checked out", async () => {
    const doesUserHaveBookCheckedOutMock =
      doesUserHaveBookCheckedOut as jest.Mock;
    const getAvailableBookIdByISBNMock = getAvailableBookIdByISBN as jest.Mock;
    const getBookByIdMock = getBookById as jest.Mock;
    const getCheckedOutBooksByUserIdMock =
      getCheckedOutBooksByUserId as jest.Mock;
    const getOverdueBooksByUserIdkMock = getOverdueBooksByUserId as jest.Mock;
    const checkOutBookMock = checkOutBook as jest.Mock;

    doesUserHaveBookCheckedOutMock.mockResolvedValue(0);
    getAvailableBookIdByISBNMock.mockResolvedValue(2);
    getBookByIdMock.mockResolvedValue(mockBookData);
    getCheckedOutBooksByUserIdMock.mockResolvedValue(1);
    getOverdueBooksByUserIdkMock.mockResolvedValue(0);

    checkOutBookMock.mockResolvedValue(mockCheckOutData);

    const actual = await handler(mockCheckOutBookRequest);

    expect(actual).toEqual({ data: mockCheckOutData });
  });

  it("should return a 400 response when required fields are missing", async () => {
    const event = {
      ...mockCheckOutBookRequest,
    };
    event.body = {};

    const actual = await handler(event);

    expect(actual).toEqual({
      message: "Missing required fields",
    });
  });

  it("should return a 409 response when the user already has the book checked out", async () => {
    const doesUserHaveBookCheckedOutMock =
      doesUserHaveBookCheckedOut as jest.Mock;
    const return409Mock = return409 as jest.Mock;
    doesUserHaveBookCheckedOutMock.mockResolvedValue(true);
    return409Mock.mockResolvedValue({
      error:
        "Unable to check out book, you already have this book checked out.",
    });
  });

  it("should return a 409 response when there are no books for the ISBN to check out", async () => {
    const getAvailableBookIdByISBNMock = getAvailableBookIdByISBN as jest.Mock;
    const return409Mock = return409 as jest.Mock;
    getAvailableBookIdByISBNMock.mockResolvedValue(true);
    return409Mock.mockResolvedValue({
      error: "This book is not available for checking out at this time",
    });
  });

  it("should return a 409 response when the user has already checked out the maximum number of books", async () => {
    const getCheckedOutBooksByUserIdMock =
      getCheckedOutBooksByUserId as jest.Mock;
    const return409Mock = return409 as jest.Mock;
    getCheckedOutBooksByUserIdMock.mockResolvedValue(3);
    return409Mock.mockResolvedValue({
      error:
        "You have 3 books checked out and are unable to check out more. Please return your books to check out more.",
    });

    const actual = await handler(mockCheckOutBookRequest);

    expect(actual).toEqual({
      error:
        "You have 3 books checked out and are unable to check out more. Please return your books to check out more.",
    });
  });

  it("should return a 409 response when the user has overdue books", async () => {
    const getOverdueBooksByUserIdMock = getOverdueBooksByUserId as jest.Mock;
    const return409Mock = return409 as jest.Mock;
    getOverdueBooksByUserIdMock.mockResolvedValue(1);
    return409Mock.mockResolvedValue({
      error:
        "You have at least one book overdue. You need to return the book(s) before you can check out more books.",
    });

    const actual = await handler(mockCheckOutBookRequest);

    expect(actual).toEqual({
      error:
        "You have at least one book overdue. You need to return the book(s) before you can check out more books.",
    });
  });

  it("should return a 500 response when an error occurs during book checkout", async () => {
    const doesUserHaveBookCheckedOutMock =
      doesUserHaveBookCheckedOut as jest.Mock;
    const getAvailableBookIdByISBNMock = getAvailableBookIdByISBN as jest.Mock;
    const getBookByIdMock = getBookById as jest.Mock;
    const getCheckedOutBooksByUserIdMock =
      getCheckedOutBooksByUserId as jest.Mock;
    const getOverdueBooksByUserIdkMock = getOverdueBooksByUserId as jest.Mock;
    const checkOutBookMock = checkOutBook as jest.Mock;

    doesUserHaveBookCheckedOutMock.mockResolvedValue(0);
    getAvailableBookIdByISBNMock.mockResolvedValue(2);
    getBookByIdMock.mockResolvedValue(mockBookData);
    getCheckedOutBooksByUserIdMock.mockResolvedValue(1);
    getOverdueBooksByUserIdkMock.mockResolvedValue(0);
    checkOutBookMock.mockResolvedValue(false);

    const actual = await handler(mockCheckOutBookRequest);

    expect(actual).toEqual({
      error:
        "An error occurred while checking out your book, please try again.",
    });
  });
});
