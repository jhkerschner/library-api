import { return200, return400, return500 } from "../../common/http";
import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { getAllCheckedOutBooksByUserId } from "../../common/accessors/transactions";
import { BookModel } from "../../common/models/book";

export async function handler(
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> {
  if (typeof event === "undefined") {
    throw new Error("Event is not defined");
  }

  if (typeof event.headers.userId === "undefined") {
    return return400({ message: "A user ID is required" });
  }
  const userId: number = +event.headers.userId;

  const checkedOutBooks: Array<BookModel> =
    await getAllCheckedOutBooksByUserId(userId);

  if (checkedOutBooks) {
    return return200({ data: checkedOutBooks });
  } else {
    return return500({
      error:
        "An error occurred while checking out your book, please try again.",
    });
  }
}
