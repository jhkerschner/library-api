import { getAllBooks } from "../../common/accessors/books";
import { getAllUser } from "../../common/accessors/users";
import { return200 } from "../../common/http";

export async function handler() {
  const allBooks = await getAllBooks();
  return return200({ data: allBooks });
}
