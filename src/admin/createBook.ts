import { return200, return400, return500 } from "../../common/http";
import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { BookCreate, BookModel } from "../../common/models/book";
import { validateInput } from "../../helpers/validation";
import { createBook } from "../../common/accessors/books";

export async function handler(
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> {
  if (typeof event === "undefined") {
    throw new Error("Event is not defined");
  }

  let book: BookCreate | null;
  if (typeof event.body === "string") {
    try {
      book = JSON.parse(event.body);
    } catch (error) {
      throw new Error(String(error));
    }
  } else {
    book = event.body;
  }

  if (!book) {
    throw new Error("body is a required argument");
  }

  if (!validateInput(book, BookCreate)) {
    return return400({ message: "Missing required fields," });
  }

  const newBook: Array<BookModel> = await createBook(book);

  if (newBook) {
    return return200({ data: newBook });
  } else {
    return return500({
      error: "An error occurred while saving the book, please try again.",
    });
  }
}
