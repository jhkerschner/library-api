import { return204, return400, return500 } from "../../common/http";
import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { deleteBookById } from "../../common/accessors/books";

export async function handler(
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> {
  if (typeof event === "undefined") {
    throw new Error("Event is not defined");
  }

  if (!event.pathParameters) {
    return return400({ error: "pathParameters not set" });
  }

  if (typeof event.pathParameters.id === "undefined") {
    return return400({ error: "id is not set in pathParameters" });
  }

  const bookId: number = +event.pathParameters.id;

  const deleteBook: any = await deleteBookById(bookId);

  if (deleteBook) {
    return return204({ data: {} });
  } else {
    return return500({
      error: "An error occurred while deleting the book, please try again.",
    });
  }
}
