import { handler } from "./createBook";
import { createBook } from "../../common/accessors/books";
const mockCreateBookRequest: any = require("../../mock/createBook.mock.json");

const mockBookData = {
  id: 4,
  isbn: "123-4567890123",
  author: "Test Author",
  title: "Test Book Title",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum elementum metus sit amet enim luctus gravida. Aenean vitae velit congue, congue lacus vel, fermentum orci. In urna elit, consectetur non sagittis non, hendrerit id elit. Vivamus eu ex vel elit dapibus dignissim.",
};

jest.mock("../../common/accessors/books", () => {
  return {
    createBook: jest.fn(),
  };
});

jest.mock("../../common/http", () => {
  return {
    return200: (): any => {
      return { data: [mockBookData] };
    },
    return400: (): any => {
      return { message: "Missing required fields" };
    },
    return500: (): any => {
      return {
        error: "An error occurred while saving the book, please try again.",
      };
    },
  };
});

describe("handler", () => {
  it("should return a 200 response with the new book", async () => {
    const createBookMock = createBook as jest.Mock;
    createBookMock.mockResolvedValue([mockBookData]);

    const actual: any = await handler(mockCreateBookRequest);
    expect(actual).toEqual({ data: [mockBookData] });
  });

  it("should return a 400 response when validation fails", async () => {
    const event = {
      ...mockCreateBookRequest,
    };
    event.body = { bookId: 1 };

    const createBookMock = createBook as jest.Mock;
    createBookMock.mockResolvedValue([mockBookData]);

    const actual = await handler(event);

    expect(actual).toEqual({
      message: "Missing required fields",
    });
  });

  it("should return a 500 response when an error occurs during book creation", async () => {
    const createBookMock = createBook as jest.Mock;
    createBookMock.mockResolvedValue(false);

    const actual = await handler(mockCreateBookRequest);

    expect(actual).toEqual({
      error: "An error occurred while saving the book, please try again.",
    });
  });
});
