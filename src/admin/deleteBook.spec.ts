import { handler } from "./deleteBook"; // Replace with the correct import path
import { deleteBookById } from "../../common/accessors/books";
const mockDeleteBookRequest: any = require("../../mock/deleteBook.mock.json");

jest.mock("../../common/accessors/books", () => {
  return {
    deleteBookById: jest.fn(),
  };
});

jest.mock("../../common/http", () => {
  return {
    return204: (): any => {
      return { data: {} };
    },
    return400: (): any => {
      return { error: "id is not set in pathParameters" };
    },
    return500: (): any => {
      return {
        error: "An error occurred while deleting the book, please try again.",
      };
    },
  };
});

describe("handler", () => {
  it("should return a 204 response when a book is successfully deleted", async () => {
    const deleteBookMock = deleteBookById as jest.Mock;
    deleteBookMock.mockResolvedValue(true);

    const actual = await handler(mockDeleteBookRequest);

    expect(actual).toEqual({ data: {} });
  });

  it("should return a 400 response when id is not set in pathParameters", async () => {
    const event = {
      ...mockDeleteBookRequest,
      pathParameters: {},
    };

    const actual = await handler(event);

    expect(actual).toEqual({ error: "id is not set in pathParameters" });
  });

  it("should return a 500 response when an error occurs during book deletion", async () => {
    const deleteBookMock = deleteBookById as jest.Mock;
    deleteBookMock.mockResolvedValue(false); // Simulate a deletion error

    const actual = await handler(mockDeleteBookRequest);

    expect(actual).toEqual({
      error: "An error occurred while deleting the book, please try again.",
    });
  });
});
