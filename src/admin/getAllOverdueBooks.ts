import { return200, return500 } from "../../common/http";
import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { getAllOverdueBooks } from "../../common/accessors/transactions";
import { BookModel } from "../../common/models/book";

export async function handler(
  event: APIGatewayEvent
): Promise<APIGatewayProxyResult> {
  if (typeof event === "undefined") {
    throw new Error("Event is not defined");
  }
  const allOverDueBooks: Array<BookModel> = await getAllOverdueBooks();

  if (allOverDueBooks) {
    return return200({ data: allOverDueBooks });
  } else {
    return return500({
      error:
        "An error occurred while retrieving overdue books, please try again.",
    });
  }
}
