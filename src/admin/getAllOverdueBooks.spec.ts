import { handler } from "./getAllOverdueBooks"; // Replace with the correct import path
import { getAllOverdueBooks } from "../../common/accessors/transactions";
const mockAllOverdueRequest: any = require("../../mock/getAllOverdueBooks.mock.json");

const mockBookData = [
  {
    id: 4,
    isbn: "123-4567890123",
    author: "Test Author",
    title: "Test Book Title",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum elementum metus sit amet enim luctus gravida. Aenean vitae velit congue, congue lacus vel, fermentum orci. In urna elit, consectetur non sagittis non, hendrerit id elit. Vivamus eu ex vel elit dapibus dignissim.",
  },
  {
    id: 10,
    isbn: "123-4567890123",
    author: "Test Author",
    title: "Test Book Title",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum elementum metus sit amet enim luctus gravida. Aenean vitae velit congue, congue lacus vel, fermentum orci. In urna elit, consectetur non sagittis non, hendrerit id elit. Vivamus eu ex vel elit dapibus dignissim.",
  },
];

jest.mock("../../common/accessors/transactions", () => {
  return {
    getAllOverdueBooks: jest.fn(),
  };
});

jest.mock("../../common/http", () => {
  return {
    return200: (): any => {
      return { data: [] }; // Adjust the data as needed
    },
    return500: (): any => {
      return {
        error:
          "An error occurred while retrieving overdue books, please try again.",
      };
    },
  };
});

describe("handler", () => {
  it("should return a 200 response with overdue books", async () => {
    const getAllOverdueBooksMock = getAllOverdueBooks as jest.Mock;
    getAllOverdueBooksMock.mockResolvedValue(mockBookData);

    const actual = await handler(mockAllOverdueRequest);

    expect(actual).toEqual({ data: [] });
  });

  it("should return a 500 response when an error occurs during book retrieval", async () => {
    const getAllOverdueBooksMock = getAllOverdueBooks as jest.Mock;
    getAllOverdueBooksMock.mockResolvedValue(false);
    const actual = await handler(mockAllOverdueRequest);

    expect(actual).toEqual({
      error:
        "An error occurred while retrieving overdue books, please try again.",
    });
  });
});
