export interface responseObject {
  body: string;
  headers: {
    "Access-Control-Allow-Headers": string;
    "Access-Control-Allow-Origin": string;
    "Access-Control-Allow-Methods": string;
  };
  statusCode: number;
}

export function return200(body?: unknown): responseObject {
  return returnHttp(200, body);
}

export function return204(body?: unknown): responseObject {
  return returnHttp(204, body);
}

export function return400(body?: unknown): responseObject {
  return returnHttp(400, body);
}

export function return401(body?: unknown): responseObject {
  return returnHttp(401, body);
}

export function return403(body?: unknown): responseObject {
  return returnHttp(403, body);
}

export function return404(body?: unknown): responseObject {
  return returnHttp(404, body);
}

export function return409(body?: unknown): responseObject {
  return returnHttp(409, body);
}

// Unprocessable Entity - You Done Messed Up A-Aron!
export function return422(body?: unknown): responseObject {
  return returnHttp(422, body);
}

export function return500(body?: unknown): responseObject {
  return returnHttp(500, body);
}

export function returnHttp(statusCode: number, body?: unknown): responseObject {
  return {
    body: JSON.stringify(body),
    headers: {
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Methods": "*",
      "Access-Control-Allow-Origin": "*",
    },
    statusCode,
  };
}
