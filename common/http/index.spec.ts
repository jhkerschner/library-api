/* eslint-disable max-lines-per-function */
import {
  responseObject,
  return200,
  return401,
  return403,
  return404,
  return422,
  return500,
  returnHttp,
} from './index';

const mockBody: any = {
  shipClasses: [
    'Sovereign',
    'Galaxy',
    'Ambassador',
    'Excelsior',
    'Constitution',
    'Prometheus',
  ],
  show: 'Star Trek',
};

describe('Http Helpers', () => {
  it('should return 200', () => {
    const actual: any = return200(mockBody);

    const expected: responseObject = {
      body: JSON.stringify(mockBody),
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Origin': '*',
      },
      statusCode: 200,
    };

    expect(actual).toEqual(expected);
  });

  it('should return 401', () => {
    const actual: any = return401(mockBody);

    const expected: responseObject = {
      body: JSON.stringify(mockBody),
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Origin': '*',
      },
      statusCode: 401,
    };

    expect(actual).toEqual(expected);
  });

  it('should return 403', () => {
    const actual: any = return403(mockBody);

    const expected: responseObject = {
      body: JSON.stringify(mockBody),
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Origin': '*',
      },
      statusCode: 403,
    };

    expect(actual).toEqual(expected);
  });

  it('should return 404', () => {
    const actual: any = return404(mockBody);

    const expected: responseObject = {
      body: JSON.stringify(mockBody),
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Origin': '*',
      },
      statusCode: 404,
    };

    expect(actual).toEqual(expected);
  });

  it('should return 422', () => {
    const actual: any = return422(mockBody);

    const expected: responseObject = {
      body: JSON.stringify(mockBody),
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Origin': '*',
      },
      statusCode: 422,
    };

    expect(actual).toEqual(expected);
  });

  it('should return 500', () => {
    const actual: any = return500(mockBody);

    const expected: responseObject = {
      body: JSON.stringify(mockBody),
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Origin': '*',
      },
      statusCode: 500,
    };

    expect(actual).toEqual(expected);
  });

  it('should return 200 manually', () => {
    const actual: any = returnHttp(200, mockBody);

    const expected: responseObject = {
      body: JSON.stringify(mockBody),
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Origin': '*',
      },
      statusCode: 200,
    };

    expect(actual).toEqual(expected);
  });
});
