import Transactions from "../models/transaction";
import User from "../models/user";

export async function getAllUser() {
  const users = await User.findAll();
  if (users) {
    return User.toData(users);
  }
}
