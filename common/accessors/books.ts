import { ValidationError } from "sequelize";
import Book, { BookCreate, BookModel } from "../models/book";
import Transactions from "../models/transaction";

export async function getAllBooks() {
  const books = await Book.findAll();
  if (books) {
    return Book.toData(books);
  } else {
    throw new Error("No Books Found");
  }
}

export async function getBookById(bookId: number): Promise<Array<BookModel>> {
  try {
    const book: BookModel | null = await Book.findByPk(bookId);

    if (book) {
      return Book.toData([book]);
    } else {
      return [];
    }
  } catch (error) {
    console.error("Error getting book by ID:", error);
    throw error;
  }
}

export async function getBookCountByISBN(isbn: string): Promise<number> {
  try {
    const bookCount = await Book.count({
      where: { isbn: isbn },
    });

    return bookCount;
  } catch (error) {
    console.error("Error getting book count by ISBN:", error);
    throw error;
  }
}

export async function createBook(data: any): Promise<Array<BookModel>> {
  try {
    const newBook: BookModel = await Book.create(data);
    if (newBook) {
      return Book.toData([newBook]);
    } else {
      throw new Error("Unable to create new book.");
    }
  } catch (error: any) {
    throw new Error(`Error creating new book: ${error.message}`);
  }
}

export async function updateBook(
  bookId: number,
  updatedData: Partial<BookModel>
): Promise<Book | null> {
  try {
    const book = await Book.findByPk(bookId);
    if (!book) {
      throw new Error(`Book with ID ${bookId} not found`);
    }

    // Update the book's attributes with the provided data
    await book.update(updatedData);

    // Return the updated book
    return book;
  } catch (error) {
    console.error("Error updating the book:", error);
    throw error;
  }
}

export async function deleteBookById(bookId: number): Promise<boolean> {
  try {
    // Use Sequelize's destroy method to delete the book by its ID
    const deletedRows = await Book.destroy({
      where: {
        id: bookId,
      },
    });

    if (deletedRows === 0) {
      // If no rows were deleted, the book was not found
      throw new Error(`Book with ID ${bookId} not found.`);
    }
    return true;
  } catch (error: any) {
    // Handle any errors that may occur during the deletion process
    throw new Error(`Error deleting the book: ${error.message}`);
  }
}
