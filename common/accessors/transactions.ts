import { Op } from "sequelize";
import Transactions, { TransactionModel } from "../models/transaction";

import Book, { BookModel, CheckedOutBookModel } from "../models/book";
import { getBookById, getBookCountByISBN, updateBook } from "./books";
import {
  convertMillisecondsToDate,
  getCurrentDate,
} from "../../helpers/date.helper";
import User from "../models/user";

export async function getAllTransactions() {
  const transactions = await Transactions.findAll();
  if (transactions) {
    return Transactions.toData(transactions);
  }
}

export async function getCheckedOutBooksByUserId(
  userId: number
): Promise<number> {
  try {
    const checkedOutBooks = await Transactions.count({
      where: {
        userId,
        returned: false,
      },
    });
    return checkedOutBooks;
  } catch (error) {
    throw new Error(`Error while retrieving checked-out books: ${error}`);
  }
}

export async function getOverdueBooksByUserId(userId: number): Promise<number> {
  try {
    const currentDate = getCurrentDate();

    const overdueBooksCount = await Transactions.count({
      where: {
        userId,
        dueBackDate: {
          [Op.lt]: currentDate,
        },
      },
    });

    return overdueBooksCount;
  } catch (error) {
    console.error("Error getting overdue books:", error);
    throw error;
  }
}

export async function doesUserHaveBook(
  isbn: string,
  userId: number
): Promise<number> {
  try {
    const count = await Transactions.count({
      where: {
        isbn: isbn,
        userId: userId,
        returned: false, // Only count transactions where the book hasn't been returned
      },
    });

    return count;
  } catch (error) {
    console.error("Error checking if the user has the book:", error);
    throw error;
  }
}
export async function checkOutBook(data: any): Promise<CheckedOutBookModel> {
  try {
    const newTransaction: TransactionModel = await Transactions.create(data);
    if (newTransaction) {
      const transaction: Array<TransactionModel> | null = Transactions.toData([
        newTransaction,
      ]);
      let book: Array<BookModel> | null = await getBookById(data.bookId);
      if (transaction && book) {
        return {
          id: book[0].id,
          isbn: book[0].isbn,
          author: book[0].author,
          title: book[0].title,
          description: book[0].description,
          checkOutDate: convertMillisecondsToDate(transaction[0].checkOutDate),
          dueBackDate: convertMillisecondsToDate(transaction[0].dueBackDate),
        };
      } else {
        throw new Error("Unable to check out book.");
      }
    } else {
      throw new Error("Unable to check out book.");
    }
  } catch (error: any) {
    throw new Error(`Error checking out book: ${error.message}`);
  }
}

export async function returnBookByIsbnAndUserId(
  isbn: string,
  userId: number
): Promise<boolean> {
  try {
    const updatedTransaction = await Transactions.update(
      { returned: true },
      {
        where: { isbn, userId },
        returning: true,
      }
    );

    if (updatedTransaction) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.error("Error returning book:", error);
    throw error;
  }
}

export async function getAllCheckedOutBooksByUserId(
  userId: number
): Promise<BookModel[]> {
  try {
    const checkedOutBooks = await Transactions.findAll({
      where: {
        userId: userId,
        returned: false,
      },
      include: {
        model: Book,
        as: "book",
      },
    });

    return checkedOutBooks.map((transaction) => transaction.book);
  } catch (error) {
    console.error("Error getting checked-out books by user:", error);
    throw error;
  }
}

export async function isBookCheckedOut(isbn: string): Promise<boolean> {
  try {
    const transaction = await Transactions.findOne({
      where: {
        isbn: isbn,
        returned: false,
      },
    });

    return !!transaction;
  } catch (error) {
    console.error("Error checking if the book is checked out:", error);
    throw error;
  }
}

export async function getAllOverdueBooks(): Promise<Array<BookModel>> {
  try {
    const currentDate = getCurrentDate();

    const overdueBooks = await Transactions.findAll({
      where: {
        dueBackDate: { [Op.lt]: currentDate },
        returned: false,
      },
      include: [
        {
          model: Book,
          as: "book",
        },
        {
          model: User,
          as: "user",
        },
      ],
    });

    return overdueBooks.map((transaction) => ({
      id: transaction.book.id,
      isbn: transaction.book.isbn,
      author: transaction.book.author,
      title: transaction.book.title,
      description: transaction.book.description,
      checkOutDate: convertMillisecondsToDate(transaction.checkOutDate),
      dueBackDate: convertMillisecondsToDate(transaction.dueBackDate),
      username: transaction.user.username,
    }));
  } catch (error) {
    console.error("Error getting overdue books:", error);
    throw error;
  }
}

export async function doesUserHaveBookCheckedOut(
  userId: number,
  isbn: string
): Promise<number> {
  try {
    const unreturnedTransaction = await Transactions.count({
      where: {
        userId: userId,
        isbn: isbn,
        returned: false,
      },
    });

    return unreturnedTransaction; // Returns true if an unreturned transaction exists, false otherwise
  } catch (error) {
    console.error("Error checking if the user has a book checked out:", error);
    throw error;
  }
}

export async function getAvailableBookIdByISBN(
  isbn: string
): Promise<number | null> {
  try {
    const unreturnedTransactionIds = await Transactions.findAll({
      where: {
        returned: false,
      },
      attributes: ["bookId"],
      include: [
        {
          model: Book,
          as: "book",
          where: { isbn: isbn },
        },
      ],
    });

    const idsOfBooksThatAreOut = unreturnedTransactionIds.map(
      (transaction) => transaction.bookId
    );

    const availableBook = await Book.findOne({
      where: {
        isbn: isbn,
        id: { [Op.notIn]: idsOfBooksThatAreOut },
      },
    });

    return availableBook ? availableBook.id : null;
  } catch (error) {
    console.error("Error getting available book ID by ISBN:", error);
    throw error;
  }
}
