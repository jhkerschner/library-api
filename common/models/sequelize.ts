import { Sequelize } from "sequelize";

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "./library.db",
  logging: false,
});

export default sequelize;
