import { DataTypes, Model, ModelStatic } from "sequelize";
import sequelize from "./sequelize";
import Transactions, { TransactionModel } from "./transaction";

export interface BookModel {
  id: number;
  isbn: string;
  author: string;
  title: string;
  description: string;
  checkedOutDate?: string;
  dueBackDate?: string;
}

export interface BookCreate {
  isbn: string;
  author: string;
  title: string;
  description: string;
}

export interface CheckedOutBookModel {
  id: number;
  isbn: string;
  author: string;
  title: string;
  description: string;
  checkOutDate: string;
  dueBackDate: string;
}

export const BookCreate = {
  isbn: "string",
  author: "string",
  title: "string",
  description: "string",
};

export interface BookCheckOut {
  isbn: string;
}

export interface BookReturn {
  isbn: string;
}

export const BookCheckOut = {
  isbn: "string",
};

export const BookReturn = {
  isbn: "string",
};

class Book extends Model {
  public id!: number;
  public isbn!: string;
  public author!: string;
  public title!: string;
  public description!: string;
  public transactions?: any;

  public static toData(books: Array<BookModel>): Array<BookModel> {
    return books.map((book) => ({
      id: book.id,
      isbn: book.isbn,
      author: book.author,
      title: book.title,
      description: book.description,
    }));
  }
}

Book.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    isbn: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isISBN(value: string): void {
          const cleanedISBN = value.replace(/[-\s]/g, "");
          const test = /^\d{13}$/.test(cleanedISBN);
          if (!test) {
            throw new Error("Invalid ISBN-13");
          }
        },
      },
    },
    author: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: "books",
    timestamps: true,
  }
);

export default Book;
