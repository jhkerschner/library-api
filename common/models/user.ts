//const { roles } = require("../../config");
import { DataTypes, Model, ModelStatic } from "sequelize";
import sequelize from "./sequelize";
import Transactions from "./transaction";

interface UserModel {
  id: number;
  username: string;
  userType: string;
}

class User extends Model {
  public id!: number;
  public username!: string;
  public userType!: string;

  public static toData(users: Array<UserModel>): Array<UserModel> {
    return users.map((user) => ({
      id: user.id,
      username: user.username,
      userType: user.userType,
    }));
  }
}

User.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userType: {
      type: DataTypes.ENUM("librarian", "user"),
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: "users",
    timestamps: true,
  }
);

export default User;
