import { DataTypes, Model, ModelStatic } from "sequelize";
import sequelize from "./sequelize";
import Book, { BookModel } from "./book";
import User from "./user";

export interface TransactionModel {
  id: number;
  bookId: number;
  isbn: string;
  userId: number;
  checkOutDate: number;
  dueBackDate: number;
  returned: boolean;
}

class Transactions extends Model {
  public id!: number;
  public bookId!: number;
  public isbn!: string;
  public userId!: number;
  public checkOutDate!: number;
  public dueBackDate!: number;
  public returned!: boolean;
  public book?: any;
  public user?: any;

  public static toData(
    users: Array<TransactionModel>
  ): Array<TransactionModel> {
    return users.map((transaction) => ({
      id: transaction.id,
      bookId: transaction.bookId,
      isbn: transaction.isbn,
      userId: transaction.userId,
      checkOutDate: transaction.checkOutDate,
      dueBackDate: transaction.dueBackDate,
      returned: transaction.returned,
    }));
  }
}

Transactions.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    bookId: {
      type: DataTypes.NUMBER,
      allowNull: false,
      references: {
        model: "books",
        key: "id",
      },
    },
    isbn: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isISBN(value: string): void {
          const cleanedISBN = value.replace(/[-\s]/g, "");
          const test = /^\d{13}$/.test(cleanedISBN);
          if (!test) {
            throw new Error("Invalid ISBN-13");
          }
        },
      },
    },
    userId: {
      type: DataTypes.NUMBER,
      allowNull: false,
      references: {
        model: "users",
        key: "id",
      },
    },
    checkOutDate: {
      type: DataTypes.NUMBER,
      allowNull: true,
    },
    dueBackDate: {
      type: DataTypes.NUMBER,
      allowNull: true,
    },
    returned: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  },
  {
    sequelize,
    tableName: "transactions",
    timestamps: true,
  }
);

Transactions.belongsTo(Book, {
  foreignKey: "bookId",
  as: "book",
});

Transactions.belongsTo(User, {
  foreignKey: "userId",
  as: "user",
});

export default Transactions;
