import Book from "./common/models/book";
import sequelize from "./common/models/sequelize";
import Transactions from "./common/models/transaction";
import User from "./common/models/user";
import { getCurrentDateAndTwoWeeksOut } from "./helpers/date.helper";

export async function handler() {
  try {
    await sequelize.authenticate();
    await sequelize.sync({ force: true });

    //Add user records
    await User.create({ username: "Librarian", userType: "librarian" });
    await User.create({ username: "User 1", userType: "user" });
    await User.create({ username: "User 2", userType: "user" });
    await User.create({ username: "User 3", userType: "user" });

    //Add book records
    await Book.create({
      isbn: "978-1492057017",
      author: "Jason Katzer",
      title:
        "Learning Serverless: Design, Develop, and Deploy with Confidence 1st Edition",
      description:
        "Whether your company is considering serverless computing or has already made the decision to adopt this model, this practical book is for you. Author Jason Katzer shows early and mid-career developers what's required to build and ship maintainable and scalable services using this model.",
    });
    await Book.create({
      isbn: "978-1492057017",
      author: "Jason Katzer",
      title:
        "Learning Serverless: Design, Develop, and Deploy with Confidence 1st Edition",
      description:
        "Whether your company is considering serverless computing or has already made the decision to adopt this model, this practical book is for you. Author Jason Katzer shows early and mid-career developers what's required to build and ship maintainable and scalable services using this model.",
    });
    await Book.create({
      isbn: "978-1492057017",
      author: "Jason Katzer",
      title:
        "Learning Serverless: Design, Develop, and Deploy with Confidence 1st Edition",
      description:
        "Whether your company is considering serverless computing or has already made the decision to adopt this model, this practical book is for you. Author Jason Katzer shows early and mid-career developers what's required to build and ship maintainable and scalable services using this model.",
    });
    await Book.create({
      isbn: "978-1492037651",
      author: "Boris Cherny",
      title:
        "Programming TypeScript: Making Your JavaScript Applications Scale 1st Edition",
      description:
        "Any programmer working with a dynamically typed language will tell you how hard it is to scale to more lines of code and more engineers. That’s why Facebook, Google, and Microsoft invented gradual static type layers for their dynamically typed JavaScript and Python code. This practical book shows you how one such type layer, TypeScript, is unique among them: it makes programming fun with its powerful static type system.",
    });

    //Add transaction recrods
    const dates = getCurrentDateAndTwoWeeksOut();

    await Transactions.create({
      bookId: 1,
      isbn: "978-1492057017",
      userId: 2,
      checkOutDate: dates.currentDate,
      dueBackDate: dates.twoWeeksOut,
    });

    await Transactions.create({
      bookId: 1,
      isbn: "978-1492057017",
      userId: 3,
      checkOutDate: "1696996800000",
      dueBackDate: "1698292799999",
    });

    console.log("Database synchronized.");
  } catch (error) {
    console.error("Error connecting to the database:", error);
  }
}
